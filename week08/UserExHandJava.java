import java.util.*;
class AgeException extends Exception{
    AgeException(String person){
        System.out.println(person);
    }
}
class Line {
        public static void main(String[] args) {
        Scanner obj=new Scanner(System.in); 
        System.out.print("Enter age of the person :"); 
        try{
            int age=obj.nextInt();
            Vote(age);
        }catch(Exception e){
            System.out.println(e);
        }finally{
            System.out.println("Try catch block executed");
        }
        }
        public static void Vote(int a) throws AgeException{
        if(a<18){
            throw new AgeException("Person is not eligible for voting");
        }
        else
        System.out.println("Person is eligible for voting");
        }
       
        
    }  
