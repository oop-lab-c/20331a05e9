#include<iostream>
using namespace std;
class Abst{
    public:
    virtual void huge() =0;
};
class Extended :Abst{
    public:
    void huge()
    {
        cout<<"show method is overrided"<<endl;
    }
};
int main()
{
    Extended obj;
    obj.huge();
    return 0;
}
