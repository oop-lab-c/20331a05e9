class Method
{
	public int base(int i)
	{
		System.out.println("Method Output : ");
		return i+3;
	}
}
class Mine extends Method
{
	public double dov(double i)
	{
		System.out.println("Mine Output : ");
		return i + 4.22;
	}
}
class Main
{
	public static void main(String args[])
	{
		Mine obj = new Mine();
		System.out.println(obj.base(4));
		System.out.println(obj.dov(5.2));
	}
}

