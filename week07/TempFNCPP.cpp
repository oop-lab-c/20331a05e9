#include<iostream>
using namespace std;
template<class T>
T addition(T &a,T &b)
{
    T res = a+b;
    return res;
}
int main()
{
    int a=8;
    int b=4;
    float c=4.2;
    float d=6.0;
    cout<<"the sum of a and b is "<<addition(a,b)<<endl;
    cout<<"the sum of c and d is "<<addition(c,d)<<endl;
    return 0;
}