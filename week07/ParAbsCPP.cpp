
#include<iostream>
using namespace std;
class A
{
    public:
    void display()
    {
        cout<<"Addition"<<endl;
    }
    virtual void add() = 0;
    
};
class B:public A
{
    public:
    virtual void wish()
    {
        cout<<"haii"<<endl;
    }
    void add()
    { 
         cout<<"enter a  value:";
        int a;
        cin>>a;
        cout<<"enter  b value:";
        int b;
        cin>>b;
        cout<<a+b<<endl;
    }
};
int main()
{
    B a;
    a.display();
    a.add();
    a.wish();
}