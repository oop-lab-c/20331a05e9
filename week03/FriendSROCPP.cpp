#include<iostream>
using namespace std;
inline void displayWelcome() //inline function
{
    cout << "Welcome" << endl;
}
class Box
{
    float length,width,height;
    public:
    void setBoxDimensions(float l,float w,float h)
    {
      length = l;
      width = w;
      height = h;
    }
     void boxVolume(float l, float w, float h);
     friend void displayBoxDimensions(Box obj);  //friend function
    void boxArea(float l, float w)
    {
        cout << "Area : " << l*w << endl;
    }
};
void Box :: boxVolume(float l,float w,float h)
{
    cout << "volume of the box : " << l*w*h <<endl;
}
void displayBoxDimensions(Box obj)
{
    cout << "length : " << obj.length << endl;
    cout << "width : " << obj.width << endl;
    cout << "height : " << obj.height << endl;
}
int main()
{
    Box obj;
    float length,width,height;
    displayWelcome();
    cout << "Enter dimensions : " << endl;
    cin >> length >> width >> height; //Taking input from user
   obj.setBoxDimensions(length,width,height);
   displayBoxDimensions(obj);
   obj.boxArea(length,width);
   obj.boxVolume(length,width,height);
    return 0;
}